package com.mkyong.hashing;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    public void testCase1()
    {
    	String expectedV = "Welcome to Java!";
    	App app = new App();
    	String actualV = app.f1();
    	assertEquals(expectedV, actualV);
    }
}
