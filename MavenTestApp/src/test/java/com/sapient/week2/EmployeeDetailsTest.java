package com.sapient.week2;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class EmployeeDetailsTest {

	private EmployeeDAO empDAO = new EmployeeDAO();
	private List<Employeebean> list = empDAO.readData();
	@Test
	void test() 
	{
		String s = "C:/Users/shrpinja/Maven Dependency/MavenTestApp/src/main/java/com/sapient/week2/";
		FileReader fileReader = null;
		try 
		{
		  fileReader = new FileReader(s+"employee.csv");
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		BufferedReader buffReader = new BufferedReader(fileReader);
		String line;
		ArrayList<Employeebean> correctlist = new ArrayList<Employeebean>();
		try 
		{
			while((line=buffReader.readLine())!=null)
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String name = data[1];
				int salary = Integer.parseInt(data[2]);
				Employeebean emp = new Employeebean(id,name,salary);
				correctlist.add(emp);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		ArrayList<Employeebean> list = (ArrayList<Employeebean>) empDAO.readData();
				
		for(int i=0;i<list.size();i++)
		{
			Employeebean emp1 = list.get(i);
			Employeebean emp2 = correctlist.get(i);
			assertEquals(emp1.equals(emp2), true);
		}
	}
	
	@Test
	void test1() throws Exception
	{
		System.out.println(empDAO.getCount(list, 850000));
		assertEquals(5, empDAO.getCount(list, 850000));
	}
	
	@Test
	void test2()throws Exception
	{
		assertEquals(9650000.0, empDAO.getTotSal(list));
	}
	
	@Test
	void test3()throws Exception
	{
		assertEquals(true, empDAO.getEmployee(1).equals(new Employeebean(1,"Shrirang",850000)));
	}
	
	

}
