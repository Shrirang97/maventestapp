package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList 
{
	List<StudentBean> list;

	public StudentList() {
		super();
		list = new ArrayList<StudentBean>();
		list.add(new StudentBean("Shrirang",21,"Mumbai"));
		list.add(new StudentBean("Tejas",21,"Mumbai"));
		list.add(new StudentBean("Sachin",21,"Mumbai"));
		list.add(new StudentBean("Sam",20,"Pune"));
		list.add(new StudentBean("Shilpa",25,"Banglore"));
	}

	public List<StudentBean> getList() {
		return list;
	}

	public void setList(List<StudentBean> list) {
		this.list = list;
	}
	
	
	
}
