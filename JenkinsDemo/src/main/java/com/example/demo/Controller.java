package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
//@Scope("prototype")
public class Controller 
{
	@Autowired
	StudentDAO sdao;
	@RequestMapping("/hello")
	public String display()
	{
		return "Welocome to Microservices!";
	}
	
	@RequestMapping("/hello1")
	public StudentBean display1()
	{
		return new StudentBean("Shrirang",21,"Mumbai");
	}
	
	@RequestMapping("/students")
	public List<StudentBean> getStudents()
	{
		return sdao.getStudents();
	}
	
	@RequestMapping("/students/{name}")
	public List<StudentBean> getDetails(@PathVariable String name)
	{
		return sdao.getDetails(name);
	}
	@RequestMapping("/students/{age}/{city}")
	public List<StudentBean> getDetailsByAgeCity(@PathVariable String age,@PathVariable String city)
	{
		return sdao.getDetailsByAgeCity(Integer.parseInt(age), city);
	}
	
	@RequestMapping("/students/range/{age1}/{age2}")
	public List<StudentBean> getDetailsByAgeRange(@PathVariable String age1,@PathVariable String age2)
	{
		return sdao.getDetailsByAgeRange(Integer.parseInt(age1), Integer.parseInt(age2));
	}
	
    @PostMapping("/students")
	//@RequestMapping(method=RequestMethod.POST,value="/students")
	public String insert(@RequestBody StudentBean s)
	{
		return sdao.insert(s);
	}
    
    @DeleteMapping("/Students")
    public String delete(@RequestBody StudentBean s)
    {
    	return sdao.deleteStudent(s);
    }
    
    
}
