package com.example.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JenkinsDemoApplicationTests {

	private String url="http://10.151.61.237:8075";
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void display()
	{
		RestTemplate restTemp = new RestTemplate();
		String actual = restTemp.getForObject(url+"/hello", String.class);
		String expected = "Welocome to Microservices!";
		assertEquals(expected, actual);
	}
	@Test
	public void display1()
	{
		RestTemplate restTemp = new RestTemplate();
		StudentBean actual = restTemp.getForObject(url+"/hello1", StudentBean.class);
		System.out.println(actual);
		String expected = "Student [name=Shrirang, age=21, city=Mumbai]";
		assertEquals(expected, actual.toString());
	}

}
